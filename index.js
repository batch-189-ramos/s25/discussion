// alert("Hello, Batch-189!")

/*
	JSON Objects
		JSON stands for Javascript Object Notation.
		JSON is also used in other programming languages hence the name Javascript Object Notatoin.

	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}

*/

/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}

// JSON Array
"cities" = [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}
	{"city": "Manila", "province": "Metro Manila", "country": "Philippines"}
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
]
*/
// JSON Methods
	// The JSON object contains methods for parsing and converting data into a stringified JSON
	// used only for transmitting data
	// light weight

// Converting data into a stringified JSON

let batchsArr = [
	{batchName: "Batch 189"	},
	{batchName: "Batch 190"	}

]


console.log(batchsArr);

// Stringify Method is used to convert JS objects into a string. Before sending data, we convert array or an object to its string equivalent.
	// stingify
console.log("Result from a stringified method:");
console.log(JSON.stringify(batchsArr));

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
})

console.log(data);

// User Details
/*let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city belong to?"),
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);
*/
// Convert stringified JSON into JS Objects
	// JSON.pars()

let batchesJSON = `[
	{
		"batchName": "Batch 189"
	},
	{
		"batchName": "Batch 190"
	}

]`

console.log(batchesJSON);

// Upon receiving our data, the JSON text can be converted back to JS Object so that we can use it in our program.
console.log("Result of parse method:");
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "Ivy",
	"age": "18",
	"address": {
		"city": "Caloocan City",
		"country": "Philippines"
	}
}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));
// In building application we almost always use "object data type"

// stringify is front to back end
// pars is from back end to front end

































